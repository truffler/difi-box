const HDWalletProvider = require('@truffle/hdwallet-provider');
//const { USBWalletProvider } = require('./usb-wallet-provider');

module.exports = {
  // See <https://trufflesuite.com/docs/truffle/reference/configuration/#networks>
  // to customize your Truffle configuration!
  // contracts_build_directory: path.join(__dirname, "app/src/contracts"),
  networks: {
    localhost: {
      host: "127.0.0.1",
      port: 8546,
      gasPrice: 5e9,
      network_id: "*" // Match any network id
    },
    ganache: {
      host: "127.0.0.1",
      port: 8545,
      gasPrice: 5e9,
      network_id: "*" // Match any network id
    },
    bsc: {
      provider: () => new HDWalletProvider('', `https://kor-node.cosmicc.online`),
      network_id: 56,
      gasPrice: 6e9,
      skipDryRun: true,
      networkCheckTimeout: 600000
    }
  },
  mocha: {
    // npm install -g mochawesome
    reporter: 'mochawesome'
  },
  compilers: {
    solc: {
      version: "0.8.10",
      docker: false,
      settings: {
        optimizer: {
          enabled: true,
          runs: 20000
        },
      }
    }
  }
};