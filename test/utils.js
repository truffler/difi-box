const slog = require('single-line-log').stdout;

const OneDay = 86400;

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

async function AfterMinerBlock(c) {
    for (let i = 0; i < c; i++) {
        await sleep(1, web3.currentProvider.send({
            jsonrpc: "2.0",
            method: "evm_mine",
            params: [],
            id: i,
        }, () => {
        }));
    }
}

async function EVMDelaySec(s) {

    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_increaseTime",
        params: [s],
        id: Math.round(new Date() / 1000),
    }, () => {

    });

    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_mine",
        params: [],
        id: Math.round(new Date() / 1000),
    }, () => {

    });
}

async function EVMDelayDay(d) {

    var s = d * OneDay;

    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_increaseTime",
        params: [s],
        id: Math.round(new Date() / 1000),
    }, () => {

    });

    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_mine",
        params: [],
        id: Math.round(new Date() / 1000),
    }, () => {

    });
}

var snapshotStack = new Array();

async function PushSnapShot() {
    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_snapshot",
        params: [],
        id: Math.round(new Date() / 1000),
    }, (err, snapshotID) => {
        snapshotStack.push(snapshotID.result);
    });
}

async function PopSnapShot() {
    var snid = snapshotStack.pop();
    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_revert",
        params: [snid],
        id: Math.round(new Date() / 1000),
    }, () => {

    });
}

async function PopSnapShotRoot() {
    var rootSNID = snapshotStack[0];
    snapshotStack = new Array();
    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_revert",
        params: [rootSNID],
        id: Math.round(new Date() / 1000),
    }, () => {

    });
}

async function RevertSnapShotAt(id) {
    await web3.currentProvider.send({
        jsonrpc: "2.0",
        method: "evm_revert",
        params: [id],
        id: Math.round(new Date() / 1000),
    }, () => {

    });
}

// 封装的 ProgressBar 工具
function ProgressBar(description, taskSum) {

    // 两个基本参数(属性)
    this.description = description || 'Progress';    // 命令行开头的文字信息
    this.length = 25;           // 进度条的长度(单位：字符)，默认设为 25
    this.total = taskSum;
    this.completed = 0;

    // 刷新进度条图案、文字的方法
    this.render = function () {
        var percent = (this.completed / this.total).toFixed(4);  // 计算进度(子任务的 完成数 除以 总数)
        var cell_num = Math.floor(percent * this.length);        // 计算需要多少个 █ 符号来拼凑图案

        // 拼接黑色条
        var cell = '';
        for (var i = 0; i < cell_num; i++) {
            cell += '█';
        }

        // 拼接灰色条
        var empty = '';
        for (var i = 0; i < this.length - cell_num; i++) {
            empty += '░';
        }

        // 拼接最终文本
        var cmdText = (100 * percent).toFixed(2) + '% ' + cell + empty + ' ' + this.description;

        // 在单行输出文本
        slog("\t⏳ \x1B[90m" + cmdText + "\x1B[39m");
    };

    this.done = function () {
        slog("");
    }

    this.completeOnce = function () {
        this.completed++;
        this.render();
        if (this.completed >= this.total) {
            slog("");
        }
    }
}

async function RunInSnapShot(f) {
    try {
        await PushSnapShot();
        await f();
    } catch (e) {
        throw e;
    } finally {
        await PopSnapShot();
    }
}

async function RunInStorage(f) {
    try {
        await f();
    } catch (e) {
        throw e;
    } finally {

    }
}


exports.pushSnapShot = PushSnapShot
exports.popSnapShot = PopSnapShot
exports.popSnapShotRoot = PopSnapShotRoot
exports.revertSnapShotAt = RevertSnapShotAt
exports.evmDelaySec = EVMDelaySec
exports.evmDelayDay = EVMDelayDay
exports.progressBar = ProgressBar
exports.atSnapShot = RunInSnapShot
exports.atStorage = RunInStorage
exports.afterMinningBlock = AfterMinerBlock
