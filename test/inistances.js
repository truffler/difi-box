const ANTNFT = artifacts.require("ANTNFT");
const AuctionHouse = artifacts.require("AuctionHouse");
const BAHToken = artifacts.require("BAHToken");
const Consensus = artifacts.require("Consensus");
const Examiner = artifacts.require("Examiner");
const Family = artifacts.require("Family");
const Genesis = artifacts.require("Genesis");
const LAHPool = artifacts.require("LAHPool");
const LAHToken = artifacts.require("LAHToken");
const MasterNFTPool = artifacts.require("MasterNFTPool");
const Oracle = artifacts.require("Oracle");
const Random = artifacts.require("Random");
const SlaveNFTPool = artifacts.require("SlaveNFTPool");
const CloseFeeRouter = artifacts.require("CloseFeeRouter");
const USDToken = artifacts.require("USDToken");
const Shareholders = artifacts.require("Shareholders");
const NFTSelector = artifacts.require("NFTSelector");
const MerkleAirDrop = artifacts.require("MerkleAirDrop");

const PancakeFactory = artifacts.require("IPancakeFactory");
// const PancakeRouter = artifacts.require("IPancakeRouter");
// const IERC20 = artifacts.require("IERC20");

async function deployed() {
    let router = await CloseFeeRouter.deployed();
    factory = await PancakeFactory.at(await router.factory());

    return {
        usd: await USDToken.deployed(),
        nft: await ANTNFT.deployed(),
        house: await AuctionHouse.deployed(),
        bah: await BAHToken.deployed(),
        consensus: await Consensus.deployed(),
        examiner: await Examiner.deployed(),
        family: await Family.deployed(),
        genesis: await Genesis.deployed(),
        lahPool: await LAHPool.deployed(),
        lah: await LAHToken.deployed(),
        master: await MasterNFTPool.deployed(),
        oracle: await Oracle.deployed(),
        random: await Random.deployed(),
        slave: await SlaveNFTPool.deployed(),
        shareholders: await Shareholders.deployed(),
        selector: await NFTSelector.deployed(),
        airdrop: await MerkleAirDrop.deployed(),
        router: router,
        factory: factory
    }
}

exports.deployed = deployed;