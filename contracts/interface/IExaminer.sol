// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IExaminer {
    function checked(
        address sender,
        address from,
        address to,
        uint256 amount
    ) external;
}
