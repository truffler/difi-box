// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IFamily {
    /// @notice 根地址
    function rootAddress() external view returns (address);

    /// @notice 地址总数
    function totalAddresses() external view returns (uint256);

    /// @notice 上级检索
    function parentOf(address child) external view returns (address);

    /// @notice 深度记录
    function depthOf(address child) external view returns (uint256);

    /**
     * @notice 直系家族(父辈)
     *
     * @dev 上级必须满足一定的邀请数量,否则将上级设置为沉淀地址
     *
     * @param user 查询的用户地址
     * @param sonsLimit 查询限制 上级的最小下级数量
     *
     * @return 地址列表(从下至上)
     *
     */
    function getFamily(
        address user,
        uint256[] memory sonsLimit,
        address preAddress
    ) external view returns (address[] memory);

    /**
     * @notice 直系家族(父辈)
     *
     * @param owner 查询的用户地址
     * @param depth 查询深度 $number
     *
     * @return 地址列表(从下至上)
     *
     */
    function getForefathers(address owner, uint256 depth)
        external
        view
        returns (address[] memory);

    /// @notice 获取直推列表
    function childrenOf(address owner) external view returns (address[] memory);

    /// @notice 下级添加上级
    function makeRelation(address parent) external;
}
