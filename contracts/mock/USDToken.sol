// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import {ERC20Burnable} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract USDToken is ERC20, ERC20Permit, ERC20Burnable {
    constructor(uint256 totalSupply)
        ERC20("USDT Mock Token", "USDT")
        ERC20Permit("USDT Mock Token")
    {
        _mint(msg.sender, totalSupply);
    }
}
