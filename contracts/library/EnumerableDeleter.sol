// SPDX-License-Identifier: Apache-2.0
pragma solidity ^0.8.0;

library EnumerableDeleter {
    function containElement(uint256[] storage self, uint256 e)
        internal
        view
        returns (bool)
    {
        for (uint256 i = 0; i < self.length; i++) {
            if (self[i] == e) {
                return true;
            }
        }

        return false;
    }

    function pushElement(uint256[] storage self, uint256 e) internal {
        require(
            !EnumerableDeleter.containElement(self, e),
            "already existed element"
        );

        self.push(e);
    }

    function removeElement(uint256[] storage self, uint256 e) internal {
        for (uint256 i = 0; i < self.length; i++) {
            if (self[i] == e) {
                for (uint256 j = i; j < self.length - 1; j++) {
                    self[j] = self[j + 1];
                }
                delete self[self.length - 1];
                self.pop();
                return;
            }
        }

        revert("not found remove element");
    }
}
